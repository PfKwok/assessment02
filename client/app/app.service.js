(function(){
    angular
        .module("GroceryApp")
        .service("GroceryAppAPI", [
            '$http',
            GroceryAppAPI
        ]);
    
    function GroceryAppAPI($http){
        var self = this;

        self.searchGroceries = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/groceries?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getGrocery = function(id){
            console.log(id);
            return $http.get("/api/groceries/" + id)
        }

        self.updateGrocery = function(grocery){
            console.log(grocery);
            return $http.put("/api/groceries", grocery);
        }

        self.deleteGrocery = function(id){
            console.log(id);
            return $http.delete("/api/groceries/" + id);
        }
        
        self.addGrocery = function(grocery){
            console.log(grocery);
            return $http.post("/api/groceries", grocery);
        }

    }
})();