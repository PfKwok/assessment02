(function () {
    angular
        .module("GroceryApp")
        .controller("GroceryController", GroceryController)
        .controller("EditGroceryCtrl", EditGroceryCtrl)
        .controller("AddGroceryCtrl", AddGroceryCtrl)
        .controller("DeleteGroceryCtrl", DeleteGroceryCtrl);
        
    GroceryController.$inject = ['GroceryAppAPI', '$uibModal', '$document', '$scope'];
    EditGroceryCtrl.$inject = ['$uibModalInstance', 'GroceryAppAPI', 'items', '$rootScope', '$scope'];
    AddGroceryCtrl.$inject = ['$uibModalInstance', 'GroceryAppAPI', 'items', '$rootScope', '$scope'];
    DeleteGroceryCtrl.$inject = ['$uibModalInstance', 'GroceryAppAPI', 'items', '$rootScope', '$scope'];

    function DeleteGroceryCtrl($uibModalInstance, GroceryAppAPI, items, $rootScope, $scope){
        console.log("Delete Grocery Ctrl");
        var self = this;
        self.deleteGrocery = deleteGrocery;
        console.log(items);
        GroceryAppAPI.getGrocery(items).then((result)=>{
            console.log(result.data);
            self.grocery =  result.data;
        });

        function deleteGrocery(){
            console.log("Delete grocery ...");
            GroceryAppAPI.deleteGrocery(self.grocery.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddGroceryCtrl($uibModalInstance, GroceryAppAPI, items, $rootScope, $scope){
        console.log("Add Grocery Ctrl");
        var self = this;
        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("Save grocery ...");
            console.log(self.grocery.upc12);
            console.log(self.grocery.brand);
            console.log(self.grocery.name);
            GroceryAppAPI.updateGrocery(self.grocery).then((result)=>{
                console.log("Add grocery -> " + result.id);
                $rootScope.$broadcast('refreshGroceryListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }

    }
        
    function EditGroceryCtrl($uibModalInstance, GroceryAppAPI, items, $rootScope, $scope){
        console.log("Edit Grocery Ctrl");
        var self = this;
        self.items = items;

        GroceryAppAPI.getGrocery(items).then((result)=>{
           console.log(result.data);
           self.grocery =  result.data;
        });

        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("Save grocery ...");
            console.log(self.grocery.upc12);
            console.log(self.grocery.brand);
            console.log(self.grocery.name);
            GroceryAppAPI.updateGrocery(self.grocery).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }

    function GroceryController(GroceryAppAPI, $uibModal, $document, $scope) {
        var self = this;
    //  self.format = "M/d/yy h:mm:ss a";
        self.groceries = [];

        self.maxsize = 5;
        self.totalItems = 0;
        self.itemsPerPage = 20;
        self.currentPage = 1;

        self.searchGroceries =  searchGroceries;
        self.pageChanged = pageChanged;

        self.editGrocery = editGrocery;
        self.addGrocery =  addGrocery;
        self.deleteGrocery = deleteGrocery;


        function searchGroceries(){
            console.log("Searching groceries .... ");
            console.log(self.orderby);
            searchAllGroceries(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        }

        function searchAllGroceries(searchKeyword, orderby, itemsPerPage, currentPage){
            GroceryAppAPI.searchGroceries(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.groceries = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems / self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }

        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGroceries(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }


        $scope.$on("refreshGroceryList", function(){
            console.log("Refresh grocery list " + self.searchKeyword);
            searchAllGroceries(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshGroceryListFromAdd", function(event, args){
            console.log("Refresh grocery list from ID " + args.id);
            var groceries = [];
            groceries.push(args);
            self.searchKeyword = "";
            self.groceries = groceries;
        });


        function editGrocery(id, size, parentSelector){
            console.log("Editing Grocery ...");
            console.log("ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editGrocery.html',
                controller: 'EditGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function addGrocery(size, parentSelector){
            console.log("Adding Grocery ...");
            var items = [];

            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addGrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

        function deleteGrocery(id, size, parentSelector){
            console.log("Deleting Grocery ...");
            console.log("ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteGrocery.html',
                controller: 'DeleteGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();