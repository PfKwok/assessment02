module.exports = function(connection, Sequelize) {
    
    var Groceries = connection.define('grocery_list', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.INTEGER(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        timestamps: false
    });
    
    return Groceries;
}